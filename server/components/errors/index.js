/**
 * Created by Michael on 11/10/2014.
 */
'use strict';

module.exports = {
  404: function pageNotFound(req, res) {
    var viewFilePath = '404';
    var result = {
      status: 404
    };

    res.status(result.status);
    res.render(viewFilePath, function (err, html) {
      if (err) {
        return res.json(result, result.status)
      } // if rendering failed, send json 404
      res.send(html);
    });
  }
};