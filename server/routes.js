/**
 * Created by Michael on 11/10/2014.
 *
 * Main application routes
 */
'use strict';

var errors = require('./components/errors');
var apiUtils = require('./api/utils');

module.exports = function (app) {

  // Application API routes
  app.use('/api/users', require('./api/users'));
  app.use('/api/campaigns', require('./api/campaigns'));

  app.use('/auth', require('./auth'));
  // Undefined asset or api routes return a 404 page
  app.route('/:url(api|auth|components|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes back to index.html
  app.route('/*')
    .get(function (req, res) {
      res.sendFile(app.get('appPath') + '/index.html');
      console.log('return index.html');
    });

  // Handle API errors
  app.use(function (err, req, res, next) {
    if (err) {
      return apiUtils.handleApiError(res, err);
    } else {
      next();
    }
  });
};
