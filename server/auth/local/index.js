/**
 * Created by Michael on 11/12/2014.
 */
'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');
var messages = require('../../config/messages');

var router = express.Router();

// /auth/local
router
  .post('/', function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
      var error = err || info;
      if (error) {
        return res.status(400).json(error);
      }
      if (!user) {
        return res.status(404).json({message: messages.UNKNOWN_ERROR});
      }
      // there were no errors and the user is here, the authentication was valid
      // create a token and send it back to the client so that sessions may persist
      var token = auth.signToken(user._id, user.role);
      res.json({token: token});
    })(req, res, next); // run the function returned by passport.authenticate as it can be used as middleware
  })

  .use(function (req, res, next) {
    res.json(404, messages.INVALID_API_VERB);
  });
module.exports = router;
