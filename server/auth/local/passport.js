/**
 * Created by Michael on 11/12/2014.
 */
'use strict';

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var messages = require('../../config/messages');

// passport.authenticate('local', ...) is not usable. (see ./index.js)
module.exports.setup = function (User, config) {
  passport.use(new LocalStrategy({
      usernameField: 'email',   // defaults to 'username'
      passwordField: 'password' // defaults to 'password' (not actually necessary to include)
    }, function (email, password, done) {
      User.findOne({
        email: email.toLowerCase()
      }, function (err, user) {
        if (err) { // unexpected error
          return done(err);
        }

        if (!user) { // the user document wasn't found the the DB, so the email hasn't been registered
          return done(null, false, {message: messages.auth.UNREGISTERED_EMAIL});
        }
        else if (!user.authenticate(password)) { // the user was found, but the password is incorrect
          return done(null, false, {message: messages.auth.INVALID_PASSWORD});
        }
        else {
          return done(null, user);
        }
      });
    }
  ));
};