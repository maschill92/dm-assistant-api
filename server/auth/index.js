/**
 * Created by Michael on 11/12/2014.
 */
'use strict';

var express = require('express');
//var passport = require('passport');
var config = require('../config/environment');
var User = require('../api/users/user.model');

// Passport Configuration
require('./local/passport').setup(User, config);

var router = express.Router();
// /auth/local*
router.use('/local', require('./local'));

module.exports = router;