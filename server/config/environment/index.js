/**
 * Created by michael on 11/8/14.
 */
'use strict';

var path = require('path');
var _ = require('lodash');

// All config will extend these options
// ====================================

var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../../'),

  // The port on which the server runs (defaults to 9000)
  port: process.env.PORT || 9000,

  // MongoDB connection options
  mongo: {
    options: {}
  },

  // Secret for session, TODO: Make an environment variable
  secrets: {
    session: 'application-secret'
  },

  // List of user roles.  Note: the order is important (least power -> most power) (see auth.service.js)
  userRoles: ['guest', 'user', 'admin']
};

module.exports =
  _.merge(all, require('./' + process.env.NODE_ENV + '.js') || {});