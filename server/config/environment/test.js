/**
 * Created by Michael on 11/13/2014.
 */
'use strict';

// Test specific configuration
module.exports = {
  port: 9001,
  mongo: {
    uri: 'mongodb://localhost/dm-assistant-test'
  }
};