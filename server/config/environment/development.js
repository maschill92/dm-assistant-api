/**
 * Created by michael on 11/8/14.
 */
module.exports = {
  // Use development MongoDB database
  mongo: {
    uri: 'mongodb://localhost/dm-assistant-dev'
  }

  // do you want to seed default, generic users?
  //seedUsers: true
};