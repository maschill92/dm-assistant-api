/**
 * Created by michael on 11/8/14.
 */
'use strict';

var config = require('./environment');
var express = require('express');

var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var session = require('express-session');
var mongoStore = require('connect-mongo')(session);
var mongoose = require('mongoose');
var morgan = require('morgan');
var path = require('path');
var errorHandler = require('errorhandler');

// Configure all Express middleware and seech!
module.exports = function (app) {
  var env = app.get('env');
  app.set('views', config.root + '/server/views');    // set directory path for views that will be rendered (note this isn't client/angular stuff)
  app.engine('html', require('ejs').renderFile);      // use ejs' renderFile function to render *.html files in the '/server/views/' directory
  app.set('view engine', 'html');                     // use 'html' as extension when extension is omitted

  app.use(compression());                             // compress responses if they are big enough (1kb default)
  app.use(bodyParser.urlencoded({extended: false}));  // parse x-ww-form-urlencoded request bodies without the Qs syntax
  app.use(bodyParser.json());                         // parse JSON request bodies
  app.use(methodOverride());                          // allows for <input type="hidden" name="_method" value="..."/> along with <form method="...">
  app.use(cookieParser());                            // read cookies from header and add to request (req.cookies)
  app.use(passport.initialize());                     // use passport as middleware

  // Persist sessions with mongoStore
  // We need to enable sessions for passport twitter because its an oauth 1.0 strategy
  app.use(session({
    secret: config.secrets.session,
    resave: true,
    saveUninitialized: true,
    store: new mongoStore({mongoose_connection: mongoose.connection})
  }));

  if (env === 'production') {
    // do favicon stuff
    // set express static path ('/public' ?)
    // set appPath ('/public' ?)

  } else if (env === 'development' || env === 'test') {
    // app.use(require('connect-livereload')());
    app.use(express.static(
      path.join(config.root, '.tmp')          // use /.tmp/ as a static folder
    ));
    app.use(express.static(
      path.join(config.root, 'client')        // use /client/ as a static folder
    ));
    app.set('appPath', config.root + 'client'); // not an express middleware, but used by /server/routes.js
    app.use(errorHandler());                    // Error handler - has to be last
  }

  if (env !== 'test') {
    app.use(morgan('dev'));                     // use morgan logger in 'dev' or 'production' mode
  }
};
