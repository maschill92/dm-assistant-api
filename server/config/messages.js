/**
 * Created by Michael on 11/13/2014.
 */
'use strict';

var messages = {
  auth: {
    INVALID_PASSWORD: 'The username or password entered is not recognized.',
    UNREGISTERED_EMAIL: 'The username or password entered is not recognized.'
  },
  UNKNOWN_ERROR: 'Something went wrong, please try again.',
  INVALID_API_VERB: {
    message: 'This API verb is not supported.'
  }
};

module.exports = messages;
