/**
 * Created by Michael on 11/13/2014.
 */
'use strict';

var User = require('../api/users/user.model');

// remove all existing users
User.find({}).remove(function () {
  User.create({
      provider: 'local',
      firstName: 'Testy',
      lastName: 'McTest',
      email: 'test@test.com',
      password: 'test'
    }, {
      provider: 'local',
      role: 'admin',
      firstName: 'Addy',
      lastName: 'Adminson',
      email: 'admin@admin.com',
      password: 'admin'
    }, function () {
      console.log('finished seeding users');
    }
  );
});