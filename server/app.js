/**
 * Created by michael on 11/8/14.
 */
'use strict';

// set environment (defaults to 'development')
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

mongoose.connect(config.mongo.uri, config.mongo.options);

if (config.seedUsers) {
  require('./config/seed');
}

// Setup server
var app = express();
var server = require('http').createServer(app);
require('./config/express')(app);               // configure all express application settings and middleware
require('./routes')(app);                       // configure all routes for the application

// Start the server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;