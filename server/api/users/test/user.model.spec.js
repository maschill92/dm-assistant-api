/**
 * Created by Michael on 11/13/2014.
 */
'use strict';

var should = require('should');
var app = require('../../../app');
var User = require('./../user.model.js');

var user;

describe('User Model', function () {
  before(function (done) {
    User.remove().exec().then(function () {
      done();
    });
  });

  beforeEach(function (done) {
    user = new User({
      provider: 'local',
      firstName: 'Fake',
      lastName: 'User',
      email: 'test@test.com',
      password: 'password'
    });
    done();
  });

  afterEach(function (done) {
    User.remove().exec().then(function () {
      done();
    });
  });

  it('should begin with no users', function (done) {
    User.find({}, function (err, users) {
      users.should.have.length(0);
      done();
    });
  });

  it('should fail when saving a duplicate user', function (done) {
    user.save(function () {
      var userDup = new User(user);
      userDup.save(function (err) {
        should.exist(err);
        done();
      });
    });
  });

  it('should fail when saving without an email', function (done) {
    user.email = '';
    user.save(function (err) {
      should.exist(err);
      done();
    });
  });

  it('should not be able to save a user with an invalid email address', function (done) {
    user.email = 'invalid@@email.com';
    user.save(function (err) {
      should.exist(err);
      done();
    });
  });

  it('should authenticate user if password is valid', function () {
    return user.authenticate('password').should.be.true;
  });

  it('should not authenticate user if password is not valid', function () {
    return user.authenticate('invalid').should.not.be.true;
  });
});
