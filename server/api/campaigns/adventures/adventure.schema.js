/**
 * Created by michael on 11/27/14.
 */
'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AdventureSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  notes: String,
  active: {
    type: Boolean,
    default: true
  }
});

module.exports = AdventureSchema;

