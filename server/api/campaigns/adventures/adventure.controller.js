/**
 * Created by michael on 11/27/14.
 */
'use strict';

var _ = require('lodash');
var apiUtil = require('./../../utils');
var Campaign = require('./../campaign.model');

// Returns all Adventures created by the requester
module.exports.index = function (req, res, next) {
  return res.status(200).json(req.campaign.adventures);
};

// Deletes an Adventure from the DB
module.exports.destroy = function (req, res, next) {
  var adventure = req.campaign.adventures.id(req.params.adventureId);
  if (!adventure) {
    return res.status(404).json(apiUtil.responses.resourceNotFound);
  }
  adventure.remove();
  req.campaign.save(function (err, campaign) {
    if (err) {
      return next(err);
    }
    return res.status(200).json(campaign);
  });
};

// Updates an existing Adventure.  Acts like PATCH in that
// it merges the selected Adventure with he given data.
module.exports.update = function (req, res, next) {
  if (req.body._id) {
    delete req.body._id;
  }
  var adventure = req.campaign.adventures.id(req.params.adventureId);
  if (!adventure) {
    return res.status(404).json(apiUtil.responses.resourceNotFound);
  }
  adventure = _.merge(adventure, req.body);
  req.campaign.save(function (err, campaign) {
    if (err) {
      return next(err);
    }
    return res.status(200).json(campaign.adventures.id(req.params.adventureId));
  });
};

// Returns the selected Adventure
module.exports.show = function (req, res, next) {
  var adventure = req.campaign.adventures.id(req.params.adventureId);
  if (!adventure) {
    return res.status(404).json(apiUtil.responses.resourceNotFound);
  }
  return res.status(200).json(adventure);
};

// Creates an Adventure
module.exports.create = function (req, res, next) {
  var adventure = req.body;
  req.campaign.adventures.push(adventure);
  req.campaign.save(function (err, campaign) {
    if (err) {
      return next(err);
    }
    return res.status(201).json(campaign);
  });
};

module.exports.attachCampaign = function (req, res, next) {
  Campaign.findById(req.params.campaignId, function (err, campaign) {
    if (err) {
      return next(err);
    }
    if (!campaign) {
      return res.status(404).json(apiUtil.responses.resourceNotFound);
    }
    req.campaign = campaign;
    next();
  });
};