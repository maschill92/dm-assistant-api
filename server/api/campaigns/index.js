/**
 * Created by michael on 11/19/14.
 */
'use strict';

var express = require('express');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');
var campaignController = require('./campaign.controller');
var adventureController = require('./adventures/adventure.controller');

var router = express.Router();

// api/campaigns/*
router.get('/', auth.isAuthenticated(), campaignController.index);
router.delete('/:campaignId', auth.isAuthenticated(), campaignController.destroy);
router.put('/:campaignId', auth.isAuthenticated(), campaignController.update);
router.get('/:campaignId', auth.isAuthenticated(), campaignController.show);
router.post('/', auth.isAuthenticated(), campaignController.create);

// TODO: find a way to move this out this file
// api/campaigns/adventures/*
router.get('/:campaignId/adventures',                 auth.isAuthenticated(), adventureController.attachCampaign, adventureController.index);
router.delete('/:campaignId/adventures/:adventureId', auth.isAuthenticated(), adventureController.attachCampaign, adventureController.destroy);
router.put('/:campaignId/adventures/:adventureId',    auth.isAuthenticated(), adventureController.attachCampaign, adventureController.update);
router.get('/:campaignId/adventures/:adventureId',    auth.isAuthenticated(), adventureController.attachCampaign, adventureController.show);
router.post('/:campaignId/adventures',                auth.isAuthenticated(), adventureController.attachCampaign, adventureController.create);

module.exports = router;
