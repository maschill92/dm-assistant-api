/**
 * Created by michael on 11/20/14.
 */
'use strict';

var _ = require('lodash');
var apiUtil = require('../utils');
var Campaign = require('./campaign.model');

// Get list of campaigns
module.exports.index = function (req, res, next) {
  Campaign.find()
    .where('owner').equals(req.user._id)
    .populate('adventures').exec(function (err, campaigns) {
      if (err) {
        return next(err);
      }
      return res.status(200).json(campaigns);
    });
};

// Get a single campaign
module.exports.show = function (req, res, next) {
  // should the owner be hidden?
  Campaign.findById(req.params.campaignId).populate('adventures').exec(function (err, campaign) {
    if (err) {
      return next(err);
    }
    if (!campaign) {
      return res.send(404);
    }
    return res.status(200).json(campaign);
  });
};

// Creates a new campaign in the DB.
module.exports.create = function (req, res, next) {
  req.body.owner = req.user._id;
  Campaign.create(req.body, function (err, campaign) {
    if (err) {
      return next(err);
    }
    return res.status(201).json(campaign);
  });
};

// Updates an existing campaign in the DB.
module.exports.update = function (req, res, next) {
  if (req.body._id) {
    delete req.body._id;
  }
  Campaign.findById(req.params.campaignId, function (err, campaign) {
    if (err) {
      return next(err);
    }
    if (!campaign) {
      return res.status(404).json(apiUtil.responses.resourceNotFound);
    }
    var updated = _.merge(campaign, req.body);
    updated.save(function (err) {
      if (err) {
        return next(err);
      }
      return res.status(200).json(campaign);
    });
  });
};

// Deletes a campaign from the DB.
module.exports.destroy = function (req, res, next) {
  Campaign.findById(req.params.campaignId, function (err, campaign) {
    if (err) {
      return next(err);
    }
    if (!campaign) {
      return res.status(404).json(apiUtil.responses.resourceNotFound);
    }
    campaign.remove(function (err) {
      if (err) {
        return next(err);
      }
      return res.sendStatus(204);
    });
  });
};
