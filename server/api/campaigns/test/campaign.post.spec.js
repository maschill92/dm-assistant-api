/**
 * Created by Michael on 11/21/2014.
 */
'use strict';
var should = require('should');
var app = require('../../../app');
var request = require('supertest');
var User = require('../../users/user.model.js');
var Campaign = require('./../campaign.model.js');
var Auth = require('../../../auth/auth.service.js');

var token;
var ownerId;
var user = new User({
  provider: 'local',
  firstName: 'Fake',
  lastName: 'User',
  email: 'test@test.com',
  password: 'password'
});

describe('POST /api/campaigns', function () {

  before(function (done) {
    User.remove().exec().then(function () {
      user.save(function (err, usr) {
        ownerId = usr.get('id');
        token = 'Bearer ' + Auth.signToken(ownerId);
        done();
      });
    });
  });

  before(function (done) {
    Campaign.remove().exec().then(function () {
      done();
    });
  });

  after(function (done) {
    User.remove().exec().then(function () {
      done();
    });
  });

  afterEach(function (done) {
    Campaign.remove().exec().then(function () {
      done();
    });
  });

  it('should respond with an UnauthorizedError without Authorization header', function (done) {
    request(app)
      .post('/api/campaigns')
      .expect(401)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        var expectedResponse = {
          name: 'UnauthorizedError',
          message: 'No Authorization header was found'
        };
        res.body.should.eql(expectedResponse);
        done();
      });
  });

  it('should respond with a ValidationError without required fields', function (done) {
    request(app)
      .post('/api/campaigns')
      .set('Authorization', token)
      .expect(422)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        var expectedResponse = {
          message: 'Validation failed',
          name: 'ValidationError',
          errors: {
            name: {
              message: 'Path `name` is required.',
              name: 'ValidatorError',
              path: 'name',
              type: 'required'
            }
          }
        };
        res.body.should.eql(expectedResponse);
        done();
      });
  });

  it('should respond return the created resource after a successful post', function (done) {
    request(app)
      .post('/api/campaigns')
      .send({name: 'Test'})
      .set('Authorization', token)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        should.not.exist(err);
        res.body.name.should.eql('Test');
        res.body.adventures.should.eql([]);
        res.body.active.should.eql(true);
        res.body.owner.should.eql(ownerId);
        done();
      });
  });

  it('shouldn\'t allow the creation of another campaign of the same name', function (done) {
    request(app)
      .post('/api/campaigns')
      .send({name: 'Duplicate'})
      .set('Authorization', token)
      .end(function (err) {
        should.not.exist(err);
        request(app)
          .post('/api/campaigns')
          .send({name: 'Duplicate'})
          .set('Authorization', token)
          .end(function (err, res) {
            var body = res.body;
            var expectedResponse = {
              message: 'Validation failed',
              name: 'ValidationError',
              errors: {
                name: {
                  message: 'More than one campaign may not have the same name',
                  name: 'ValidatorError',
                  path: 'name',
                  type: 'user defined',
                  value: 'Duplicate'
                }
              }
            };
            body.should.eql(expectedResponse);
            done();
          })
        ;
      })
    ;
  });
});
