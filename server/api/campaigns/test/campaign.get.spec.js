/**
 * Created by Michael on 11/21/2014.
 */
'use strict';

var should = require('should');
var app = require('../../../app');
var request = require('supertest');
var User = require('../../users/user.model.js');
var Campaign = require('./../campaign.model.js');

var user = new User({
  provider: 'local',
  firstName: 'Fake',
  lastName: 'User',
  email: 'test@test.com',
  password: 'password'
});
var token;

describe('GET /api/campaigns', function () {

  before(function (done) {
    User.remove().exec().then(function () {
      user.save(function () {
        done();
      });
    });
  });

  beforeEach(function (done) {
    request(app)
      .post('/auth/local')
      .send({email: 'test@test.com', password: 'password'})
      .end(function (err, res) {
        token = 'Bearer ' + res.body.token;
        done();
      });
  });

  after(function (done) {
    User.remove().exec().then(function () {
      done();
    });
  });

  it('should respond with an UnauthorizedError without Authorization header', function (done) {
    request(app)
      .get('/api/campaigns')
      .expect(401)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        var expectedResponse = {
          name: 'UnauthorizedError',
          message: 'No Authorization header was found'
        };
        res.body.should.eql(expectedResponse);
        done();
      });
  });

  it('should respond with an Array', function (done) {
    request(app)
      .get('/api/campaigns')
      .set('Authorization', token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        res.body.should.be.instanceof(Array);
        done();
      });
  });
});
