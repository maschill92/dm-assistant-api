'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var AdventureSchema = require('./adventures/adventure.schema');

var CampaignSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  notes: String,
  active: {
    type: Boolean,
    default: true
  },
  owner: {
    type: ObjectId,
    ref: 'User',
    required: true
  },
  adventures: [AdventureSchema]
});

/**
 * Validations
 */
// Validate that a user can't have the more
// than one campaign with the same name
CampaignSchema
  .path('name')
  .validate(function (name, respond) {
    var document = this;
    document.constructor
      .where('owner').equals(this.owner)
      .exec(function (err, campaigns) {
        if (err) {
          throw err;
        }
        for (var i = 0; i < campaigns.length; i++) {
          var campaign = campaigns[i];
          if (name === campaign.name && !document._id.equals(campaign._id)) {
            return respond(false);
          }
        }
        return respond(true);
      })
    ;
  }, 'More than one campaign may not have the same name');

// Validate that a campaign doesn't have more
// than one adventure with the same name
CampaignSchema
  .path('adventures')
  .validate(function (adventures, respond) {
    for (var i = 0; i < adventures.length; i++) {
      for (var j = i + 1; j < adventures.length; j++) {
        if (adventures[i].name === adventures[j].name && !adventures[j]._id.equals(adventures[i]._id)) {
          return respond(false);
        }
      }
    }
    respond(true);
  }, 'More than one adventure in a campaign may not have the same name.');

module.exports = mongoose.model('Campaign', CampaignSchema);