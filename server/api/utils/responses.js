/**
 * Created by michael on 11/27/14.
 */
'use strict';

module.exports = {
  resourceNotFound: {
    message: 'Resource not found'
  }
};
