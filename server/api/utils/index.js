/**
 * Created by michael on 11/27/14.
 */
'use strict';
module.exports = {
  handleApiError: function (res, err) {
    var status;
    var response;
    if (err.name === 'ValidationError') {
      status = 422;
      response = err;
    } else if (err.name === 'CastError' && err.path === '_id') {
      status = 404;
      response = {
        value: err.value,
        message: 'Resource not found.'
      };
    } else if (err.name === 'UnauthorizedError') {
      status = 401;
      response = {
        name: err.name,
        message: err.message
      };
    } else {
      status = 500;
      response = err;
    }
    return res.status(status).json(response);
  },

  responses: require('./responses')
};

