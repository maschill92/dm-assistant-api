/**
 * Created by Michael on 11/10/2014.
 */
'use strict';

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var mocha = require('gulp-mocha');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var gutil = require('gulp-util');

var paths = {
  server: {
    index: './server/app.js',
    tests: './server/**/*.spec.js'
  },
  client: {
    sassSrc: ['./client/**/*.scss', '!./client/bower_components/**'],
    sassDest: './client/css/',
    staticPublic: ['./client/**', '!./client/**/*.scss']
  }
};


/**
 * Watch for changes in client files and
 * take steps to handle the changes properly
 * */
gulp.task('watch', function () {
  livereload.listen();

  gulp.watch(paths.client.sassSrc, ['sass']);  // when the app sass file is changed, recompile it.
  gulp.watch(paths.client.staticPublic).on('change', livereload.changed);
});

/**
 * Compile sass from the sass source location and
 * put it to the sass destination location
 * */
gulp.task('sass', function () {
  return gulp
    .src(paths.client.sassSrc, {
      base: 'client/app'
    })
    .pipe(sass({
      onError: handleSassError
    }))
    .pipe(gulp.dest(paths.client.sassDest))
  ;

  function handleSassError (err) {
    gutil.log(err);
  }
});


gulp.task('serve', ['sass', 'watch'], function () {
  return nodemon({
    script: paths.server.index,
    ignore: ['client/**/*', 'gulpfile.js']
  });
});

gulp.task('test', function () {
  process.env.NODE_ENV = 'test';
  var exitCode = 0;
  return gulp
    .src(paths.server.tests)          // look for *.spec.js files EVERYWHERE in the repository
    .pipe(mocha({reporter: 'spec'}))  // run each one of those files through mocha.  Hooray unit testing!
    .on('error', function () {
      exitCode = 1;
    })
    .on('end', function () {
      process.exit(exitCode);
    });
});

gulp.task('default', ['serve']);
