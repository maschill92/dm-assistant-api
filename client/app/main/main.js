/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';
  angular.module('main', [
    'ui.router'
  ])
    .config(function ($stateProvider) {
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'app/main/main.tmpl.html',
          controller: 'MainCtrl'
        });
    });

}());