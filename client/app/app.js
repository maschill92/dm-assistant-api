/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';
  angular.module('dma', [
    'ngCookies',
    'ngResource',
    'ui.router',
    'dma.components.services.Auth',
    'dma.components.directives.dmaNavbar',
    'main',
    'account',
    'campaignManager'
  ])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
      $urlRouterProvider.otherwise('/');
      $locationProvider.html5Mode(true);
      $httpProvider.interceptors.push('authInterceptor'); // see below
    })

    .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
      return {
        // Add authorization token to headers if found
        request: function (config) {
          config.headers = config.headers || {};
          if ($cookieStore.get('token')) {
            config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
          }
          return config;
        },

        // Intercept 401s and redirect you to login
        responseError: function (response) {
          if (response.status === 401) {
            $location.path('/account/login');
            // remove any stale tokens
            $cookieStore.remove('token');
            return $q.reject(response);
          }
          else {
            return $q.reject(response);
          }
        }
      };
    })

    .run(function ($rootScope, $state, Auth) {
      // Redirect to login if route requires auth and you're not logged in
      $rootScope.$on('$stateChangeStart', function (event, nextState) {
        if (nextState.requiresAuthentication) {
          Auth.isLoggedInAsync(function (isLoggedIn) {
            if (nextState.requiresAuthentication && !isLoggedIn) {
              // prevent the state change and go to the login state
              event.preventDefault();
              $state.go('account.login');
            }
          });
        }
      });
    });
}());