/**
 * Created by michael on 11/27/14.
 */
(function () {
  'use strict';
  angular.module('campaignManager.list', [
    'dma.components.resources.Campaign'
  ])
    .controller('CampaignListCtrl', function (Campaign) {
      var vm = this;

      Campaign.query(function (campaigns) {
        vm.campaigns = campaigns;
      });

      vm.toggle = function (campaign) {
        campaign.collapsed = !campaign.collapsed;
      };
    });
}());