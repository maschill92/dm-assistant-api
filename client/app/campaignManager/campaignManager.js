/**
 * Created by Michael on 11/24/2014.
 */
(function () {
  'use strict';
  angular.module('campaignManager', [
    'ui.router',
    'ui.tree',
    'campaignManager.list'
  ])
    .config(function ($stateProvider) {
      $stateProvider
        .state('campaignManager', {
          url: '/campaignManager',
          requiresAuthentication: true,
          views: {
            '@': {
              templateUrl: 'app/campaignManager/campaignManager.tmpl.html'
            },
            'campaignList@campaignManager': {
              templateUrl: 'app/campaignManager/campaignList/campaignList.tmpl.html',
              controller: 'CampaignListCtrl as campaignList'
            },
            'mainWindow@campaignManager': {
              templateUrl: 'app/campaignManager/main.html'
            }
          }
        })
        .state('campaignManager.edit', {
          url: '/edit',
          views: {
            'mainWindow': {
              template: '<div>BOOM</div>'
            }
          }
        })
      ;
    })
  ;
}());