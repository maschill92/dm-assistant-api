/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';
  angular.module('account')
    .controller('SignupCtrl', function ($scope, Auth, $state) {
      var vm = this;
      vm.user = {};
      vm.errors = {};
      vm.register = register;

      function register(form) {
        vm.submitted = true;

        if (form.$valid/* && password === confimPassword*/) {
          Auth.createUser({
            firstName: vm.user.firstName,
            lastName: vm.user.lastName,
            email: vm.user.email,
            password: vm.user.password
          })
            .then(function () {
              // Account created, redirect to home
              $state.go('campaignManager');
            })
            .catch(function (err) {
              err = err.data;

              // Update validity of form fields that match the errors from the API
              angular.forEach(err.errors, function (error, field) {
                vm.errors[field] = error.message;
                form[field].$invalid = true;
              });
            });
        } else {
          handleInvalidSignupForm(form);
        }
      }

      function handleInvalidSignupForm(form) {
        vm.errors = {};
        if (form.firstName.$error.required) {
          vm.errors.firstName = 'First Name is required.';
        }
        if (form.lastName.$error.required) {
          vm.errors.lastName = 'Last Name is required.';
        }
        if (form.email.$error.required) {
          vm.errors.email = 'Email is required.';
        } else if (form.email.$error.email) {
          vm.errors.email = 'Please enter a valid email.';
        }
        if (form.password.$error.required) {
          vm.errors.password = 'Please enter a password.';
        } else if (form.password.$modelValue !== form.confirmPassword.$modelValue) {
          vm.errors.confirmPassword = 'The passwords must match!';
        }
      }
    });
}());
