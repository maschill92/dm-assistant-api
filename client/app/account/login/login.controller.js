/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';
  angular.module('account')
    .controller('LoginCtrl', function ($scope, Auth, $state, $log) {
      var vm = this;
      vm.user = {};
      vm.errors = {};
      vm.login = login;

      function login(form) {
        vm.submitted = true;

        if (form.$valid) {
          Auth.login({
            email: vm.user.email,
            password: vm.user.password
          })
            .then(function () {
              // Logged in, redirect to dashboard
              $log.info('User: ' + vm.user.email + ' successfully logged in.');
              $state.go('campaignManager');
            })
            .catch(function (err) {
              vm.errors.other = err.message;
            });
        } else {
          handleInvalidLoginForm(form);
        }
      }

      function handleInvalidLoginForm(form) {
        vm.errors = {};
        if (form.email.$error.required) {
          vm.errors.email = 'Email is required';
        } else if (form.email.$error.email) {
          vm.errors.email = 'Please enter a valid email.';
        }
        if (form.password.$error.required) {
          vm.errors.password = 'Password is required';
        }
      }
    });
}());