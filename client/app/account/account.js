/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';
  angular.module('account', [
    'ui.router',
    'dma.components.services.Auth'
  ])
    .config(function ($stateProvider) {
      $stateProvider
        .state('account', {
          url: '/account',
          abstract: true,
          template: '<div ui-view></div>'
        })
        .state('account.login', {
          url: '/login',
          templateUrl: 'app/account/login/login.html',
          controller: 'LoginCtrl as login'
        })
        .state('account.signup', {
          url: '/signup',
          templateUrl: 'app/account/signup/signup.html',
          controller: 'SignupCtrl as signup',
          controllerAs: 'signup'
        })
        .state('account.settings', {
          url: '/settings',
          templateUrl: 'app/account/settings/settings.tmpl.html',
          requiresAuthentication: true
        })
      ;
    })
  ;
}());