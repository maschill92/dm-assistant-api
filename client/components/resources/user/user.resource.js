/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';

  angular.module('dma.components.resources.User', [])
    .factory('User', function ($resource) {
      return $resource('/api/users/:id/:controller', {
          id: '@_id'
        },
        {
          changePassword: {
            method: 'PUT',
            params: {
              controller: 'password'
            }
          },
          get: {
            method: 'GET',
            params: {
              id: 'me'
            }
          }
        });
    });
}());
