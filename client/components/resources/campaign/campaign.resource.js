/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';
  angular.module('dma.components.resources.Campaign', [])
    .factory('Campaign', ['$resource', function ($resource) {
      return $resource('/api/campaigns/:id', {
        id: '@_id'
      });
    }]);
}());
