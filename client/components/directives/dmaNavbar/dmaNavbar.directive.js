/**
 * Created by Michael on 11/19/2014.
 */
(function () {
  'use strict';
  angular.module('dma.components.directives.dmaNavbar', [
    'dma.components.services.Auth'
  ])
    .directive('dmaNavbar', function ($document) {
      return {
        restrict: 'E',
        templateUrl: 'components/directives/dmaNavbar/dmaNavbar.html',
        compile: function () {
          $($document).foundation();
        }
      };
    });
}());