/**
 * Created by Michael on 11/18/2014.
 */
(function () {
  'use strict';

  angular.module('dma.components.directives.dmaNavbar')
    .controller('DmaNavbarCtrl', function ($state, Auth) {
      this.isCollapsed = true;
      this.isLoggedIn = Auth.isLoggedIn;
      this.isAdmin = Auth.isAdmin;
      this.getCurrentUser = Auth.getCurrentUser;
      this.getUsername = function () {
        var currentUser = this.getCurrentUser();
        if (!this.isLoggedIn()) {
          return '';
        }
        if (currentUser.email) {
          return currentUser.email;
        }
        else if (currentUser.username) {
          return currentUser.username;
        }
        else {
          return 'User';
        }
      };
      this.logout = function () {
        Auth.logout();
        $state.go('account.login');
      };

      this.isActive = function (route) {
        return $state.is(route);
      };
    });
}());